<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatRelation extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function talkedTo()
  {
      return $this->hasMany(Chat::class,'sender_id');
  }

  public function relatedTo()
  {
      return $this->hasMany(Chat::class,'receiver_id');
  }
}
