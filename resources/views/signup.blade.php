@extends('layouts.app')
@section('content')

  <h1><center>Signup Form</center></h1>
  <h1 class="text-white">

              {{ (isset($user)) ? 'Update User Detail' : 'Add User Detail' }}
            </h1>
            @php $action = (isset($user) ? route('update',$user->id) :route('signup') ) @endphp
<form  action="{{$action}}" method="POST" enctype="multipart/form-data">
  @csrf
<label>Name</label>
<input type="text" name="name" class="form-control" value="{{ old('name', $user->name ?? '') }}" required/>
      @error('name')
           <span class="validation text-danger" role="alert">
            <strong>{{ $message }}</strong>
           </span>
       @enderror

<label>Email</label>
<input type="email" name="email" class="form-control" value="{{ old('email', $user->email ?? '') }}" required/>
    @error('email')
        <span class="validation text-danger" role="alert">
           <strong>{{ $message }}</strong>
        </span>
    @enderror
<!-- $('#data->id').hide() -->
<label>Password</label>
<input type="password" name="password" class="form-control"  required/>
       @error('password')
          <span class="validation text-danger" role="alert">
            <strong>{{ $message }}</strong>
         </span>
       @enderror
<!-- $('#id').hide() -->
<label>Confirm Password</label>
<input type="password" name="confirm_password" class="form-control" required/>
      @error('confirm_password')
            <span class="validation text-danger" role="alert">
               <strong>{{ $message }}</strong>
             </span>
      @enderror

  <label>Image</label>
      <input type="file" name="image" class="form-control" accept="image/*" required/>
            @error('image')
                  <span class="validation text-danger" role="alert">
                     <strong>{{ $message }}</strong>
                   </span>
            @enderror
<div class="mt-4">
<button type="submit">{{ (isset($user)) ? 'Update' : 'Submit' }}</button>
</div>
</form>


@endsection
