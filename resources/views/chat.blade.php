@extends('layouts.chatApp')
@section('content')

    <!-- Page header start -->
    <div class="page-title">
        <div class="row gutters">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                <h5 class="title">Chat App</h5>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12"> </div>
        </div>
    </div>
    <!-- Page header end -->

    <!-- Content wrapper start -->
    <div class="content-wrapper">

        <!-- Row start -->
        <div class="row gutters">

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                <div class="card m-0">

                    <!-- Row start -->
                    <div class="row no-gutters">
                        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-3 col-3">
                            <div class="users-container">
                                <!-- <div class="chat-search-box">
                                  <form action="{{ route('chat') }}" method="get" >
                                    <div class="input-group">
                                        <input class="form-control" name="keyword" type="text"  placeholder="Search" value="{{ request()->get('keyword') ?? '' }}">
                                        <div class="input-group-btn">
                                            <button type="submit" class="btn btn-info">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a class="btn btn-warning btn-sm" href="{{ route('chat') }}">Clear</a>
                                        </div>
                                    </div>
                                  </form>
                                </div> -->

                                <ul class="users">
                                  @foreach($users as $user)

                                    <li class="person" data-chat="person1">
                                      <a href="{{ route('chat',['id' => base64_encode($user->id)]) }}">
                                        <div class="user">
                                          <img src="{{ $user->image_url }}" class="avatar rounded-circle" alt="Retail Admin">

                                            <span class="status busy"></span>
                                        </div>
                                        <p class="name-time">
                                            <span class="name">{{ $user->name }}</span>
                                            <span class="time">15/02/2019</span>
                                        </p>
                                      </a>
                                    </li>


                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-9 col-9">
                            <div class="selected-user">
                                <span>To: <span class="name">{{ $getUser->name ?? 'user' }}</span></span>
                            </div>
                            <div class="chat-container">
                                <ul class="chat-box chatContainerScroll">

                                  <!--Reciver Message -->
                                  <!-- {{$msg ?? ''}} -->
                                  @if(isset($msg))
                                  @forelse($msg as $message)

                                  @if(Auth::id() == $message->reciver_id)
                                    <li class="chat-left">
                                        <div class="chat-avatar">
                                            <img src="https://www.bootdey.com/img/Content/avatar/avatar3.png" alt="Retail Admin">
                                            <div class="chat-name">{{ $getUser->name ?? 'reciever' }}</div>
                                        </div>
                                        <div class="chat-text" id="messages">{{$message->message ?? ''}}</div>
                                        <div class="chat-hour">08:55 <span class="fa fa-check-circle"></span></div>
                                    </li>
                                    @endif
                                      <!-- Sender Message -->
                                    @if(Auth::id() == $message->sender_id)
                                    <li class="chat-right">
                                        <div class="chat-hour">08:56 <span class="fa fa-check-circle"></span></div>
                                        <div class="chat-text" id="messages">{{$message->message ?? ''}}</div>
                                        <div class="chat-avatar">
                                            <img src="https://www.bootdey.com/img/Content/avatar/avatar3.png" alt="Retail Admin">
                                            <div class="chat-name">{{ $name->name ?? 'sender' }}</div>
                                        </div>
                                    </li>
                                    @endif
                                  @empty
                                  <p>No chat found</p>
                                  @endforelse
                                  @endif
                                </ul>
                                @include('record')
                                <div class="form-group mt-3 mb-0">
                                  <form method="post" action="{{ route('sendmessage')}}">
                                    @csrf
                                    <input type="hidden" name="reciver_id" value="{{ $getUser->id ?? '' }}">
                                    <textarea class="form-control" name="message" rows="3" placeholder="Type your message here..."></textarea>
                                    <button type="submit"><i class="fas fa-paper-plane text-success"></i></button>
                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Row end -->
                </div>

            </div>

        </div>
        <!-- Row end -->

    </div>
    <!-- Content wrapper end -->
<div class="mt-4">
  <a href="{{ route('userList') }}" class="btn btn-success">All User</a>
</div>
<div class="mt-4" style="float:right;">
  <a href="{{ route('logout') }}" class="btn btn-danger">Logut</a>
</div>

<script>
$('#recorder').on('change', function(){
    var form      = $("#voiceformpost");
    var file_data = $("#audio").prop("files")[0];
    var reciver_id = $("#reciver_id").prop("id")[0];
    var formdata  = new FormData(form);
    formdata.append('files', file_data);
    formdata.append('id', reciver_id);
    $.ajax({
        url : "{{url('sendmessage')}}",
        type: "POST",
        data : formdata,
        processData: false,
        contentType: false,
        success:function(data){
        }
    });
}, "blob");



</script>

<!-- <script>
    var socket = io.connect('http://localhost:8890');
    socket.on('message', function (data) {
        data = jQuery.parseJSON(data);
        console.log(data.user);
        $( "#messages" ).append( "<strong>"+data.user+":</strong><p>"+data.message+"</p>" );
      });
    $(".send-msg").click(function(e){
        e.preventDefault();
        var token = $("input[name='_token']").val();
        var user = $("input[name='user']").val();
        var msg = $(".msg").val();
        if(msg != ''){
            $.ajax({
                type: "POST",
                url: '{!! URL::to("sendmessage") !!}',
                dataType: "string",
                data: {'_token':token,'message':msg,'user':user},
                success:function(data){
                    console.log(data);
                    $(".msg").val('');
                }
            });
        }else{
            alert("Please Add Message.");
        }
    }) -->
</script>

@endsection
