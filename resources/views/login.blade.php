<!DOCTYPE html>
<html>
<head>
  <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<title>task</title>

<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet">
@notifyCss

<script src="jquery-3.5.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>
<body>
<div class="container">
   <h1><center>Login Form</center></h1>
<form method="post" action="{{route('login')}}">
  @csrf


<label>Email</label>
<input type="email" name="email" class="form-control" value="{{ old('email') }}" required>
    @error('email')
      <span class="validation text-danger" role="alert">
        <strong>{{ $message }}</strong>
      </span>
    @enderror

<label>Password</label>
<input type="password" name="password" class="form-control" required>
    @error('password')
       <span class="validation text-danger" role="alert">
         <strong>{{ $message }}</strong>
       </span>
    @enderror

<div class="mt-4">
<button type="submit"class="btn btn-success">Submit</button>
</div>

</form>
<div class="mt-4">
  <a href="{{route('signupForm')}}">Sign Up</a>
</div>


</div>


<x:notify-messages />
@notifyJs
</body>
</html>
