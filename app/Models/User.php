<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
     // protected $guarded = ['id'];
    protected $fillable = [
        'name',
        'email',
        'password',
        'image',
    ];

    protected $appends = [
        'image_url'
      ];

      public function getimageUrlAttribute()
   {
       if (preg_match('(https://|http://)', $this->image) === 1) {
           return $this->image;
       }
       return !empty($this->image) ? asset("images/$this->image") : asset('assets/img/user-icon.png');
   }
    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function messages() {
    return $this->hasMany(ChatRelation::class, 'reciver_id');
  }
}
