<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Models\User;
use App\Models\Message;
use Mail;
use Illuminate\Support\Facades\Hash;
use Redirect;

class TaskController extends Controller
{
    public function signupForm()
    {
      return view('signup');
    }

    public function store(Request $request)
    {

    $this->validate($request,[
            'name'    =>  'required|min:2',
            'email'    => 'required|email|unique:users,email',
            'password' => 'required|min:8',
            'confirm_password' => 'required|same:password',
            'image' => 'required'
            ]);

            $data = $request->except('_token');
            $data['password'] = Hash::make($request->password);

            if (request()->hasFile('image')) {
                $file = request()->file('image');
                $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
                $file->move(public_path('images'), $fileName);
                $data['image'] = $fileName;
            }

       User::create($data);
       notify()->success('Signup successfully');
       return redirect()->route('loginForm');

    }

    public function loginForm()
    {
      return view('login');
    }

    public function login(Request $request)
    {
      $request->validate([
            'email'    => 'required|email',
            'password' => 'required|min:8',
            ]);

      if(Auth::attempt([ 'email' => $request->email, 'password' => $request->password])){
                notify()->success('Login successfuly');
               return redirect('chat');
            }else{
              notify()->warning('invalid cerdential');
              return Redirect::back();
            }

    }

    public function messageForm()
    {
        return view('mail');
    }

    public function  message(Request $request)
    {

      $request->validate([
            'email'    => 'required|email',
            'message' => 'required|max:250',
            'file'   => 'required|mimes:pdf,jpg,jpeg,png'
            ]);

            $file= new Message();


             $filename=time().'.'.$request->file->getClientOriginalName();

                    $request->file->move(public_path('files'), $filename);





            $data['msg'] = $request->message;

          $filepath =   public_path('files/'.$filename);


           $user = User::first('email');


            Mail::send( 'message', $data, function ($message) use ($request, $filepath, $user) {
                $message->to($request->email)->from($user->email)->subject('Attachment')->attach($filepath);

          });



                  $file->file = $filename;
                  $file['email'] = $request->email;
                  $file['message'] = $request->message;
                  $file->save();

                  notify()->success('mail send successfully');
                return Redirect::back();

    }

    public function index()
    {
      $data = Message::paginate(10);
      return view('table', compact('data'));
    }
    public function userList()
    {
         $data['user'] = User::paginate(10);
        return view('user', $data);
    }

    public function edit($id)
    {
         $data['user'] = User::find(base64_decode($id));
        return view('signup', $data);
    }

    public function update(Request $request, $id)
    {
      $request->validate([
          'image'           => 'required|image|mimes:jpeg,png,jpg,gif,svg',
          'name'            => 'required|max:30',
          'email'           => 'required|',
      ]);

      $data = $request->except('_token','image');

      $user = User::find($id);

      if (request()->hasFile('image')) {
          $file = request()->file('image');
          $fileName = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
          $file->move(public_path('images'), $fileName);
          $data['image'] = $fileName;
      }

      $data['password'] = Hash::make($request->password);

      $user->update($data);
      notify()->success('User data successfuly updated');
      return redirect()->route('userList');
    }

    public function logout()
    {
      Auth::logout();
      return redirect('/login');
    }
}
