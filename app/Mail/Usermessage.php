<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Usermessage extends Mailable
{
    use Queueable, SerializesModels;

    protected $msg;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      // $msg = $this->msg['message'];

      return $this->subject('Document Upload')
                  ->to('user')
                  ->from('bhagatsinghapp@gmail.com')
                  ->view('message', compact('data'))
                  ->attach($this->message['filename']->getRealPath(),
                [
                    'as' => $this->message['filename']->getClientOriginalName()
                ]);
        // $user = User::first('email');
        // return $this->from('bhagatsinghapp@gmail.com')->view('message', compact('msg'));
    }
}
