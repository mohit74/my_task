@extends('layouts.app')
@section('content')

   <h1><center>Mail Send</center></h1>
<form method="post" action="{{route('message')}}" enctype="multipart/form-data">
  @csrf


<label>Email</label>
<input type="email" name="email" class="form-control" value="{{ old('email') }}" required/>
        @error('email')
                     <span class="validation text-danger" role="alert">
                         <strong>{{ $message }}</strong>

                     </span>
        @enderror

<label>message</label>
<textarea type="text" name="message" class="form-control" rows="4" cols="50" required/>{{ old('message') }}</textarea>
          @error('message')
                     <span class="validation text-danger" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
          @enderror

<label>File</label>
<input type="file" name="file" accept=".pdf,.doc,.jpg,.png,.jpeg" required/>
          @error('file')
                     <span class="validation text-danger" role="alert">
                         <strong>{{ $message }}</strong>
                     </span>
          @enderror

<div class="mt-4">
<button type="submit" class="btn btn-success">Submit</button>
</div>
</form>

<div class="mt-4">
  <a href="{{ route('table') }}" class="btn btn-primary">Message History</a>
</div>
<div class="mt-4" style="float:right;">
  <a href="{{ route('logout') }}" class="btn btn-danger">Logut</a>
</div>

@endsection
