<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::namespace('App\Http\Controllers')->group(function() {

        Route::get('signup', 'TaskController@signupForm')->name('signupForm');
        Route::post('signup', 'TaskController@store')->name('signup');
        Route::get('login', 'TaskController@loginForm')->name('loginForm');
        Route::post('login', 'TaskController@login')->name('login');

        Route::group(['middleware' => 'auth'], function() {

        Route::get('messageForm', 'TaskController@messageForm')->name('messageForm');
        Route::post('message','TaskController@message')->name('message');
        Route::get('index', 'TaskController@index')->name('table');
        Route::get('logout', 'TaskController@logout')->name('logout');

        Route::get('chat', 'ChatController@chat')->name('chat');
        Route::post('chat', 'ChatController@message')->name('message');
        Route::get('userList', 'TaskController@userList')->name('userList');
        Route::get('user/{id}', 'TaskController@edit')->name('edit');
        Route::post('user/{id}', 'TaskController@update')->name('update');
        Route::post('sendmessage', 'ChatController@sendmessage')->name('sendmessage');
        Route::get('audio', 'ChatController@audio')->name('audio');
      



});
});
