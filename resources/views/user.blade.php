@extends('layouts.app')
@section('content')

  <h2><center>ALL USER LIST</center></h2>

  <div class="mt-4" style="float:right;">
    <a href="{{ route('chat') }}" class="btn btn-danger">Back</a>
  </div>
  <table>
    <tr>
      <th>#</th>
      <th>Name</th>
      <th>Email</th>
      <th>Image</th>
      <th>Action</th>
    </tr>
    @foreach ($user as $users)
    <tr>

      <td>{{ $users->id }}</td>
      <td>{{ $users->name }}</td>
      <td>{{ $users->email }}</td>
      <td>{{ $users->image }}</td>
      <td> <a href="{{ route('edit', base64_encode($users->id)) }}" class="table-action" data-toggle="tooltip" data-original-title="Edit User">
                    Edit
                  </a></td>
  
      <td>

      </td>
    </tr>
    @endforeach
    {{ $user->links() }}
  </tabel>

  @endsection
