<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use App\Models\Chat;
use App\Models\ChatRelation;
// use LRedis;

class ChatController extends Controller
{
  public function chat(Request $request)
  {
    $data['search'] = User::where(function ($query) use ($request) {
      if ($request->keyword) {
          $query->where('name', 'LIKE', "%$request->keyword%");
      }
    })->get();

    $data['users'] = User::where('id', '!=', auth()->id())->get();
    $data['getUser'] = User::where('id', base64_decode($request->id))->first();
    $data['name'] = Auth::user();

    $relation = ChatRelation::where(function($q) use($request){
      $q->where(['sender_id' => Auth::id(),'reciver_id' => base64_decode($request->id)]);
    })->OrWhere(function($q) use($request){
      $q->where(['sender_id' => base64_decode($request->id),'reciver_id' => Auth::id()]);
    })->first();


    if(isset($relation))
    {

        $data['msg']=Chat::where('chat_relation_id',$relation->id)->get();
      // $data['sender_msg'] = Chat::where(function($q) use($request){
      //               $q->where('chat_relation_id',$relation->id);
      //             })->OrWhere(function($q) use($request){
      //               $q->where(['sender_id'=> Auth::id(), 'message'=> $request->message]);
      //             })->get();
    }
    // else {
    //  $data['msg'] = ;
    // }


    return view('chat', $data);

}

public function sendmessage(Request $request)
{

   $relation = ChatRelation::where(function($q) use($request){
     $q->where(['sender_id' => Auth::id(),'reciver_id' => ($request->reciver_id)]);
   })->OrWhere(function($q) use($request){
     $q->where(['sender_id' => ($request->reciver_id),'reciver_id' => Auth::id()]);
   })->first();


   if(!$relation)
   {
     $chatData['sender_id'] = Auth::id();
     $chatData['reciver_id'] = $request->reciver_id;
     $relation = ChatRelation::create($chatData);

   }

   $data['sender_id'] = Auth::id();
   $data['reciver_id'] = $request->reciver_id;
   $data['chat_relation_id'] = $relation->id;
   $data['message'] = $request->message;
//print_r($request->audio);
print_r($_FILES);
die();
$data = file_get_contents($_FILES['audio']['tmp_name']);

$fp = fopen("saved_audio/" . $_REQUEST["name"] . '.wav', 'wb');

fwrite($fp, $data);
fclose($fp);
   if (request()->hasFile('audio')) {
       $file = request()->file('audio');
       $fileName = time(). '.' . $file->getClientOriginalExtension();
      $request->$file->move(public_path('record'), $fileName);
      return $data['audio'] = $fileName;
    }


// return $request;
   Chat::create($data);
   return back();
}

public function audio(Request $request)
{
//  print_r($request->audiourl);
  //die;
  $afile=file_get_contents($request->audiourl);
// phpinfo();
// die();
  //die($request->values['audiourl']);
 //  $ch = curl_init('http://127.0.0.1:8002/images/f6facb416a5f86bba9c2585120c4bdff.jpg');
 // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
 // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
 // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
 // $data = curl_exec($ch);
 // curl_close($ch);
  $myfile = fopen(public_path('record')."/test.wav", "w") or die("Unable to open file!");
  //$txt = "John Doe\n";
  fwrite($myfile, $afile);
  //$txt = "Jane Doe\n";
  //fwrite($myfile, $txt);
  fclose($myfile);

  //echo"testing";
  //
//  $data = new Chat;
  // return $request->audio;

      //  $data->save();
    //  return back();

}

}
