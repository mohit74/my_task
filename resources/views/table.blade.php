@extends('layouts.app')
@section('content')

  <h2><center>Message History</center></h2>

  <div class="mt-4" style="float:right;">
    <a href="{{ route('messageForm') }}" class="btn btn-danger">Back</a>
  </div>
  <table>
    <tr>
      <th>#</th>
      <th>Email</th>
      <th>Message</th>
      <th>File</th>
    </tr>
    @foreach ($data as $history)
    <tr>

      <td>{{ $history->id }}</td>
      <td>{{ $history->email }}</td>
      <td>{{ $history->message }}</td>
      <td>
        <iframe src="{{ asset('files/'.$history->file) }}" width="100" height="100">
           This browser does not support PDFs. Please download the PDF to view it: <a href="{{ asset('files/'.$history->file) }}">Download PDF</a>
   </iframe>
      </td>
    </tr>
    @endforeach
    {{ $data->links() }}
  </tabel>

  @endsection
